import requests
import os
import json
import random
import decimal
from time import sleep

def save_variant_images(publication,ip, token):
    variant_folders = [variant for variant in os.listdir() if variant != 'main']

    for index, variant_folder in enumerate(sorted(variant_folders)):
        os.chdir(variant_folder)
        img_url = f"{ip}/commerce/product-publications/{publication['id']}/images"
        payload = {
            'variant_id':publication['variants']['data'][index]['id']
        }
        files = [
            (
                f'image-{index}',
                (
                    image,
                    open(f'{os.getcwd()}/{image}', 'rb'),
                    'image/jpeg' if image[-3:] == 'jpg' else 'image/png'
                )
            ) 
            for index, image in enumerate(sorted(os.listdir()))
        ]
        headers = {
            'Authorization': f'Bearer {token}'
        }

        response = requests.request("POST", img_url, headers=headers, data=payload, files=files)

        os.chdir('..')

def save_main_images(publication,ip, token):
    os.chdir('main')
    img_url = f"{ip}/commerce/product-publications/{publication['id']}/images"
    payload = {}
    files = [
        (
            f'image-{index}',
            (
                image,
                open(f'{os.getcwd()}/{image}', 'rb'),
                'image/jpeg' if image[-3:] == 'jpg' else 'image/png'
            )
        ) 
        for index, image in enumerate(sorted(os.listdir()))
    ]
    headers = {
        'Authorization': f'Bearer {token}'
    }

    response = requests.request("POST", img_url, headers=headers, data=payload, files=files)

    os.chdir('../')

def create_images(response_publication, publication_counter, ip, token):
    os.chdir(f'imgs/{publication_counter}')
    publication = requests.get(f"{ip}/publications/{response_publication.json()['id']}/details").json()

    save_main_images(publication, ip, token)

    if publication.get('variants'):
        save_variant_images(publication, ip, token)

    print(publication['id'])

    os.chdir('../..')

def send_requests(data_json, directory, token, ip):
    publication_counter = 1
    for data in data_json:
        sleep(10)
        publication_url = "{ip}/commerce/product-publications".format(ip=ip)
        payload = data
        headers = {
            'Authorization': 'Bearer {token}'.format(token=token),
            'Content-Type': 'application/json'
        }

        response_publication = requests.request("POST", publication_url, headers=headers, data=json.dumps(payload))
        print(response_publication.text)
        create_images(response_publication, publication_counter, ip, token)

        publication_counter += 1

    return

def main():
    listos = ['Memorias', 'consolas-videojuegos', 'pc', 'audifonos'] # listos local
    ip = input('pega aqui la ip de la api, ejemplo : http://192.168.1.104:3001 >>>')  # cambiar
    token = input('pega aqui el access_token >>> ') # cambiar

    directories = [directory for directory in os.listdir() if os.path.isdir(directory) and directory != '.git' and directory not in listos]

    for directory in directories:
        print(directory)

        if(input('continuar? y/n >>> ') != 'y'):
            continue

        os.chdir(directory)

        with open('data.json', 'r', encoding='utf8') as json_file:

            data_json = json.load(json_file)
            try:
                send_requests(data_json, directory, token, ip)
            except Exception as e:
                print(e)
                continue

        os.chdir('..')

    return

if __name__ == '__main__':
    main()